<?php

use Blog\Core\Router;

//todo более конкретные правила (пользовательские) находятся выше чем более общие

//default routes для admin части приложения
Router::add('^admin$', ['controller' => 'Main', 'action' => 'index', 'prefix' => 'admin']);
Router::add('^admin/?(?P<controller>[a-z-]+)/?(?P<action>[a-z-]+)?$', ['prefix' => 'admin']);


//default routes для пользовательской части приложения
Router::add('^$', ['controller' => 'Main', 'action' => 'index']);

//пишем ?P - чтобы получить ассоциативную запись объекта, а далее наименование строкового ключа в массиве <controller>
Router::add('^(?P<controller>[a-z-]+)/?(?P<action>[a-z-]+)?$');// здесь action не явл. обязательным т.к. заключен в знаки ??
