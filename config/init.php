<?php
/*
 * указываем режим development=1 (error on), prodaction =0 (error off)
 * */

define("DEBUG", 1);
define("ROOT", dirname(__DIR__));
define("WWW", ROOT . '/public');
define("APP", ROOT . '/app');
define("CORE", ROOT . '/packages/blog/core');
define("BOOT", ROOT . '/bootstrap');
define("CACHE", ROOT . '/tmp/cache');
define("CONF", ROOT . '/config');
define("VIEWS", ROOT . '/views');
define("LAYOUT", 'default');

// get url main page
$app_path = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['PHP_SELF']}";
// http://oop-system-krivchenkov/public/
$app_path = preg_replace("#[^/]+$#", '', $app_path); //ищем все символы кроме / начиная с конца строки -> замещаем пустой строкой в $app_path
// http://oop-system-krivchenkov/
$app_path = str_ireplace('/public/', '', $app_path);

define("PATH", $app_path);
define("ADMIN", PATH . '/admin');


//var_dump(PATH);

