<?php

/**
 * @author Krivchenkov SV
 * @author <kriv4enkov@gmail.com>
 * @version v 1.0  дата релиза 2021-08-11;
 */
/**
 * @var App\App $app
 */

require __DIR__ . '/../vendor/autoload.php';

$app = require_once __DIR__ . '/../bootstrap/app.php';
$app->start();

// todo necessary delete 2 file from public - index.html & post.html

echo '<hr>';

//$app::$app->setProperty('test', 'TEST!!!!');

// todo почему подчеркивает что не так?
//debug($app::$app->getProperties());

//Генерируем исключение
//throw new Exception('Страница не найдена', 505);

// выводим написанные правила для теста.
//debug(\Blog\Core\Router::getRoutes());