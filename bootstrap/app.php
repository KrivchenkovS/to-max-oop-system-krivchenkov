<?php
//ЭТО БИБЛИОТЕКА BOOTSTRAP
use App\App;


$app = new App();
$app->init();
require_once dirname(__DIR__) . '/config/init.php';
require_once BOOT . '/functions.php';
require_once dirname(__DIR__) . '/config/routes.php';

return $app;