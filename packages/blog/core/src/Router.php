<?php


namespace Blog\Core;


class Router
{
    protected static $routes = []; // набор маршрутов
    protected static $route = []; // текущий маршрут

    //записываем правило в табл маршрутов где $regexp - шаблон регулярного выражения
    public static function add($regexp, $route = [])
    {
        self::$routes[$regexp] = $route;
    }

    public static function getRoutes()
    {
        return self::$routes;
    }

    public static function getRoute()
    {
        return self::$route;
    }

    // метод принимает url адресс
    public static function dispatch($url)
    {
        $url = self::removeQueryString($url);
        if (self::matchRoute($url)) {
            // для админской части прописываем -> self::$route['prefix']
            // постфикс 'Controller' для вызыва только контроллеров
            $controller = 'App\Controllers\\' . self::$route['prefix'] . self::$route['controller'] . 'Controller';

            //todo 1. создание объекта класса: (в конструктор контороллера передаем текущий маршрут содержащий [controller] => ..., [action] => ...,[prefix] => ...) todo 2. создание action контроллера
            if (class_exists($controller)) {
// создание объекта контроллера
                $controllerObject = new $controller(self::$route);
                $action = self::lowerCamelCase(self::$route['action']) . 'Action';
                if (method_exists($controllerObject, $action)) {
// создание метода контроллера
                    $controllerObject->$action();
// вызыв метода вида
                    $controllerObject->getView();
                } else {
                    throw new \Exception("Метод $controller::$action не найден", 404);
                }
            } else {
                throw new \Exception("Контроллер $controller не найден", 404);
            }
        } else {
            throw new \Exception("Страница не найдена", 404);
        }
    }

    // метод поиска соответствия в имеющейся табл. маршрутов
    public static function matchRoute($url)
    {
        foreach (self::$routes as $pattern => $route) {
            if (preg_match("|{$pattern}|", $url, $matches)) {
                //debug($matches);
                //исключаем индексовые значения из результатов поиска соответствия
                foreach ($matches as $key => $value) {
                    if (is_string($key)) {
                        $route[$key] = $value;
                    }
                }
                //если передан только 1 сигмент в url то...
                if (empty($route['action'])) {
                    $route['action'] = 'index';
                }
                if (!isset($route['prefix'])) {
                    $route['prefix'] = '';
                } else {
                    $route['prefix'] .= '\\';
                }
                $route['controller'] = self::upperCamelCase($route['controller']);
                self::$route = $route;
                //debug(self::$route);
                return true;
            }
        }
        return false;
    }

     // - для имен контроллеров CamelCase
    protected static function upperCamelCase($name)
    {
        return str_replace(' ', '', ucwords(str_replace('-', ' ', $name)));
    }

     // - для имен экшенов camelCase
    protected static function lowerCamelCase($name)
    {
        return lcfirst(self::upperCamelCase($name));

    }

    // работа с get параметрами из url
    protected static function removeQueryString($url)
    {
        if($url){
            //делим строку запроса на 2 части по & для чего исп explode
            $params = explode('&', $url, 2);
            if(false === strpos($params[0], '=')){
                return rtrim($params[0], '/');
                }else{
                return '';
            }
        }


    }
}