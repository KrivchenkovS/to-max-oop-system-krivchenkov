<?php

namespace Blog\Core\Models;

use Blog\Core\Db;


// предназначен для работы с данными (не только из БД) различные методы, валидации, файлы ...
abstract class AbstractModel
{
    public $attributes = []; // для хранения массива свойсв модели с данными кот будет идентичен названию полей в таблице БД что загружать автоматически данные из полей в модель...
    public $errors = []; // для записи ошибок
    public $rules = []; // для правил валидации данных

    //организовываем подключение к БД
    public function __construct()
    {
        Db::instance();

    }

}