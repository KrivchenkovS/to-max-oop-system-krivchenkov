<?php


namespace Blog\Core;


class Cache
{
    use TSingletone;

    // record it too cache
    public function set($key, $data, $seconds = 3600)
    {
        if ($seconds) {
            $content['data'] = $data;
            // конечное время на которое кешируем данные
            $content['end_time'] = time() + $seconds;
            //запись данных в файл
            if (file_put_contents(CACHE . '/' . md5($key) . '.txt', serialize($content))) {
                return true;
            }
        }
        return false;
    }

    // read from the cache
    public function get($key)
    {
        $file = CACHE . '/' . md5($key) . '.txt';
        if (file_exists($file)) {
            $content = unserialize(file_get_contents($file));
            if (time() <= $content['end_time']) {
                return $content;
            }
            //удаляем файл
            unlink($file);
        }
        return false;
    }

    // delete from the cache
    public function delete($key)
    {
        $file = CACHE . '/' . md5($key) . '.txt';
        if (file_exists($file)) {
            unlink($file);
        }
    }

}