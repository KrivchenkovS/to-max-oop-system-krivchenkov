<?php


namespace Blog\Core;


class ErrorHandler
{
    public function __construct()
    {
        if (DEBUG) {
            error_reporting(-1);
        } else {
            error_reporting(0);
        }
        // обработка исключений
        set_exception_handler([$this, 'exceptionHandler']);
    }

// сюда данные передает set_exception_handler
    public function exceptionHandler($e)
    {
        $this->logErrors($e->getMessage(), $e->getFile(), $e->getLine());
        $this->displayError('Исключение', $e->getMessage(), $e->getFile(), $e->getLine(), $e->getCode());


    }

// метод логирование ошибок (3-означает запись в файл)
    protected function logErrors($message = '', $file = '', $line = '')
    {
        error_log("[" . date('Y-m-d H:i:s') . "]\n Текст ошибки: {$message} |\n Файл: {$file} |\n Строка: {$line}\n======================\n", 3, ROOT . '/tmp/errors.log');

    }

// метод вывода ошибок
    protected function displayError($errno, $errstr, $errfile, $errline, $responce = 404)
    {
        http_response_code($responce);
        if ($responce == 404 && !DEBUG) {
            require WWW . '/error/404.php';
            die;
        }
        if (DEBUG) {
            require WWW . '/error/dev.php';
        } else {
            require WWW . '/error/prod.php';
        }
        die;


    }

}