<?php


namespace Blog\Core;


class Db
{
    use TSingletone;

    protected function __construct()
    {
        //todo Connect DB_Libraries ReadBean
        $db = require_once ROOT . '/config/database.php';
        class_alias('\RedBeanPHP\R', '\R');
        \R::setup($db['dsn'], $db['user'], $db['pass']);
        if(!\R::testConnection()){
            throw new \Exception("НЕТ подключения к БД!", 500);
        } else {
            echo 'Соединение установлено !!!';
        }

        //todo === use freeze for ban automatic formation DDL comande ===

        \R::freeze(true);
        if(DEBUG){
            \R::debug(true,1);
        }
    }


}