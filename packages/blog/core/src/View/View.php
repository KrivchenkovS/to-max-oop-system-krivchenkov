<?php


namespace Blog\Core\View;


class View
{
    public $route;
    public $controller;
    public $model;
    public $view;
    public $prefix;
    public $layout;
    public $data = [];
    public $meta = [];

    public function __construct($route, $layout = '', $view = '', $meta = '')
    {
        $this->route = $route;
        $this->controller = $route['controller'];
        $this->model = $route['controller'];
        $this->view = $view;
        $this->prefix = $route['prefix'];
        $this->meta = $meta;
        if ($layout === false) {
            $this->layout = false;
        } else {
            //если передан шаблон $layout то возьмем его иначе (если передана пустая строка) значение константы LAYOUT.
            $this->layout = $layout ?: LAYOUT;
        }
    }

    // метод для передачи данных будет вызываться в контроллере
    public function render($data)
    {
        //debug($data);

        //извлекаем данные из массива (по замыслу урока) исп extract который извлечет данные из массива и сформирует соответствующее переменные...
        if(is_array($data)) extract($data);

        // формируем путь к виду
        /*
         * приводим имя директории к нижнему регистру (в видео папки в верхнем регистре)$this->controller = lcfirst($this->controller);
         * */

        $viewFile = ROOT . "/views/{$this->prefix}{$this->controller}/{$this->view}.php";

        if (is_file($viewFile)) {
//todo подключаем буферизацию
            ob_start();
            require_once $viewFile;

// в $content теперь хранится содержимое контента
            $content = ob_get_clean();
        } else {
            throw new \Exception("Не найден вид {$viewFile}", 500);
        }
        if (false !== $this->layout) {
            $layoutFile = ROOT . "/views/layouts/{$this->layout}.php";
        }
        if (is_file($layoutFile)) {
            require_once $layoutFile;
        } else {
            throw new \Exception("Не найден шаблон {$layoutFile}", 500);

        }
    }

//return разметку meta тегов для шаблоно html документа
    public function getMeta()
    {
        $output = '<title>' . $this->meta['title'] . '</title>>' . PHP_EOL;
        $output .= '<meta name="description" content="' . $this->meta['desc'] . '">' .PHP_EOL;
        $output .= '<meta name="keywords" content="' . $this->meta['keywords'] . '">'. PHP_EOL;
        return $output;

    }
}