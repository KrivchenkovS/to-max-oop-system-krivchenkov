<?php


namespace Blog\Core;

// патерн singletone (use for создать только один объект данного класса)
trait TSingletone
{
    private static $instance;
    public static function instance()
    {
        if(self::$instance === null){
            self::$instance = new self;
        }
        return self::$instance;
    }

}