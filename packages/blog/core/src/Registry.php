<?php


namespace Blog\Core;

// todo реализует singletone;
class Registry
{
    use TSingletone;
    protected static $properties = []; // сюда складываем свойства.

    public function setProperty($name, $value)
    {
        self::$properties[$name] = $value; //здесь в ключ name сохраним value;
    }

    public function getProperty($name)
    {
        if(isset(self::$properties[$name])){
            return self::$properties[$name];
        }
        return null;
    }

    public function getProperties()
    {
        return self::$properties;
    }
}