<?php

namespace Blog\Core\Controllers;

// Это контроллер framework

use Blog\Core\View\View;

abstract class AbstractController
{
    public $route;
    public $controller;
    public $model;
    public $view;
    public $prefix;
    public $layout;
    public $data = [];// нужна для того чтобы затем передавать инф в вид
    public $meta = ['title' =>'', 'desc'=> '', 'keywords'=>''];

    public function __construct($route)
    {
        $this->route = $route;
        $this->controller = $route['controller'];
        $this->model = $route['controller'];
        $this->view = $route['action'];
        $this->prefix = $route['prefix'];
    }

    // получаем вид
    public function getView()
    {
        //создаем объект вида и перед параметры в контроллер этого объекта
        $viewObject = new View($this->route, $this->layout, $this->view, $this->meta);
        // вызываем метод render что бы сформировать страницу
        $viewObject->render($this->data);

    }

    //метод для наполнения $data для ее послед отправки в view через render
    public function set($data)
    {
        $this->data = $data;
    }

    // метод для метаданных
    public function setMeta($title = '', $desc = '', $keywords = '')
    {
        $this->meta['title']=$title;
        $this->meta['desc']=$desc;
        $this->meta['keywords']=$keywords;

    }


}