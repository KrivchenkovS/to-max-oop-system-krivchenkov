<?php

namespace App\Controllers;


use App\App;
use Blog\Core\Cache;

class MainController extends AppController
{
    // public $layout = 'test';
    public function indexAction()
    {
        //$this->layout = 'test';
        //echo __METHOD__;

        $posts = \R::findAll('test');
        $post = \R::findOne('test', 'id=?', [2]);

        // передача метаданных
        $this->setMeta('This is main page', 'Description', 'Ключевики...');
        //$this->setMeta(App::$app->getProperty('name'), 'Description', 'Ключевики...');

//-------------------------------------------------
        //1 варианта передачи данных
        $name = 'John';
        $age = 36;
        $names = ['ivar', 'jane', 'eva', 'mike'];

//todo передаваемые данные выводятся в views/main/index.php
       // $this->set(compact('name', 'age', 'names', 'posts'));
        /*
         * 2 вариант передачи данных
         * $this->set(['name'=>'Andrey', 'age' => 30]);
         * */
//-------------------------------------------------

        // todo проверяем кэширование данных
        $cache = Cache::instance();
        //$cache->set('test', $names);

        //$cache->delete('test');

        $data = $cache->get('test');
        if (!$data) {
            $cache->set('test', $names);

        }
//        debug($data);

    }
}