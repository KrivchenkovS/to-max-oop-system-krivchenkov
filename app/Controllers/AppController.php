<?php


namespace App\Controllers;

use App\Models\AppModel;
use Blog\Core\Controllers\AbstractController;

// Это базовый контроллер всего приложения

//todo вероятнее всего эту же фугкцию выполняет App.php

class AppController extends AbstractController
{
    public function __construct($route)
    {
        parent::__construct($route);
        new AppModel();
    }

}